import pytest

import debts


def test_long_floats_are_truncated():
    part_1 = 10 / 3.0
    part_2 = 20 / 2.0
    part_3 = 13.33

    fred = 10 - part_1 - part_2
    alexis = 20 - part_1 - part_3
    remy = 13.33 - part_1 - part_2

    assert debts.settle([("fred", fred), ("alexis", alexis), ("rémy", remy)]) == [
        ("fred", 3.33, "alexis")
    ]


def test_simple_debts_are_solved():
    assert debts.settle([("fred", -200), ("alexis", -300), ("rémy", +500)]) == [
        ("fred", 200, "rémy"),
        ("alexis", 300, "rémy"),
    ]

    assert debts.settle(
        [
            ("camille", -155),
            ("fred", -200),
            ("alexis", -300),
            ("rémy", 300),
            ("yohann", 355),
        ]
    ) == [
        ("camille", 155.0, "rémy"),
        ("fred", 145.0, "rémy"),
        ("fred", 55.0, "yohann"),
        ("alexis", 300.0, "yohann"),
    ]


def test_big_debts_are_solved_properly():
    balance = (
        ("simon", 1134.82),
        ("pauline", 126.60),
        ("laura", 148.00),
        ("laura", 0),
        ("laura", 92),
        ("valentin", 120.96),
        ("alexis", 456.70),
        ("colin", 100),
        ("alexis", 245.35),
        ("rémy", 384),
        ("elsa", 490.12),
        ("simon", -356.76),
        ("rémy", -220.59),
        ("elsa", -150.67),
        ("alexis", -90.28),
        ("colin", -234.28),
        ("ter-ter", -298.33),
        ("chez-francine", -176.82),
        ("la-lanterne", -240.75),
        ("laura", -268.78),
        ("cece-manu", -184.85),
        ("valentin", -566.86),
        ("amour-de-coloc", -151.26),
        ("thorigné", -28.67),
        ("fleurs-bleues", -37.78),
        ("tréguinguette", -291.87),
    )
    assert debts.settle(balance) == [
        ("thorigné", 28.67, "pauline"),
        ("laura", 28.78, "pauline"),
        ("fleurs-bleues", 37.78, "pauline"),
        ("colin", 31.37, "pauline"),
        ("colin", 102.91, "rémy"),
        ("amour-de-coloc", 60.50, "rémy"),
        ("amour-de-coloc", 90.76, "elsa"),
        ("chez-francine", 176.82, "elsa"),
        ("cece-manu", 71.87, "elsa"),
        ("cece-manu", 112.98, "alexis"),
        ("la-lanterne", 240.75, "alexis"),
        ("tréguinguette", 258.04, "alexis"),
        ("tréguinguette", 33.83, "simon"),
        ("ter-ter", 298.33, "simon"),
        ("valentin", 445.90, "simon"),
    ]


def test_person_present_in_both_sides():
    # Have people appear in both - and + sides (alexis appears twice)
    assert debts.settle(
        [("fred", -200), ("alexis", -400), ("rémy", +500), ("alexis", +100)]
    ) == [("fred", 200.0, "rémy"), ("alexis", 300.0, "rémy")]


def test_multiple_crediters():
    assert debts.settle(
        [("marie", +200), ("marie", +400), ("séverine", -500), ("jeanne", -100)]
    ) == [("jeanne", 100.0, "marie"), ("séverine", 500.0, "marie")]


def test_multiple_debiters():
    assert debts.settle(
        [("marie", -200), ("marie", -400), ("séverine", +500), ("jeanne", +100)]
    ) == [("marie", 100.0, "jeanne"), ("marie", 500.0, "séverine")]


def test_unbalanced_debts_throws_an_error():
    # Unbalanced request
    with pytest.raises(debts.UnbalancedRequest):
        debts.settle([("fred", -200), ("alexis", -300), ("rémy", +400)])


def test_rounding_doesnt_mess_it_up():
    balance = {
        1: -14.485714285714286,
        2: 59.01428571428571,
        3: 4.814285714285713,
        4: -14.485714285714286,
        5: -14.485714285714286,
        6: -14.485714285714286,
        7: -5.885714285714285,
    }
    debts.settle(balance.items())
