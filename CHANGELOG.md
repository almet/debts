# 0.5 (2019-11-03)

- Do not round values before processing them.

# O.4

- Initial release.
